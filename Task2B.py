from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.stationdata import update_water_levels
from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold

# Build list of stations
stations = build_station_list()

 # Update latest level data for all stations
update_water_levels(stations)

list_over_tol = stations_level_over_threshold(stations,0.8)

for entry in list_over_tol:
    print("{}, {}".format(entry[0], entry[1]))