from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list

stations = build_station_list()

rivers_set = rivers_with_station(stations)
rivers_list = list(rivers_set)
rivers_list = sorted(rivers_list)

print (len(rivers_set))
print (rivers_list[:10])

rivers_dictionary = stations_by_river(stations)


print (rivers_dictionary['River Aire'])

print (rivers_dictionary['River Cam'])

print (rivers_dictionary['River Thames'])