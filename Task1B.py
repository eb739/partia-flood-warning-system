from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list

stations = build_station_list()

station_town_distance_list = stations_by_distance(stations,(52.2053, 0.1218))

print (station_town_distance_list[:10])

print (station_town_distance_list[-10:])

