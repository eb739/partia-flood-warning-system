from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations

stations = build_station_list()

inconsistent_stations_list = inconsistent_typical_range_stations(stations)

inconsistent_stations_list = sorted(inconsistent_stations_list)

print (inconsistent_stations_list)


