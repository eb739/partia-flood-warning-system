from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.stationdata import update_water_levels
from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold
from floodsystem.flood import stations_highest_rel_level

def test_stations_highest_rel_level():
    stations = build_station_list()
    update_water_levels(stations)
    test_list = stations_highest_rel_level(stations, 7)
    assert len(test_list) == 7
