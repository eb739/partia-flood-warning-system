from floodsystem.station import MonitoringStation
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list



def test_rivers_by_station_number():
    stations = build_station_list()
    N_rivers_list = rivers_by_station_number(stations, 16)
    assert len(N_rivers_list) >= 16