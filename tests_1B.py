from floodsystem.station import MonitoringStation
from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list


def test_stations_by_distance():

    # Create a station
    s_id = "test1B1-s-id"
    m_id = "test1B1-m-id"
    label = "1B1_test_station"
    coord = (5, 5)
    trange = (6, 3)
    river = "River 1B1"
    town = "1B1 Town"
    s1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    # Create a second station
    s_id2 = "test1B2-s-id"
    m_id2 = "test1B2-m-id"
    label2 = "1B2_test_station"
    coord2 = (2, 2)
    trange2 = (6, 3)
    river2 = "River 1B2"
    town2 = "1B2 Town"
    s2 = MonitoringStation(s_id2, m_id2, label2, coord2, trange2, river2, town2)

    stations_to_test = [s1,s2]

    # Check s2 is closer to the origin than s1
    assert stations_by_distance(stations_to_test,(0,0)) == [s2,s1]