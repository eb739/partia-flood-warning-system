from floodsystem.station import MonitoringStation
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list

stations = build_station_list()


def test_stations_by_river():

    # Create a station
    s_id = "test1D-s-id"
    m_id = "test1D-m-id"
    label = "1D_test_station"
    coord = (-2.0, 4.0)
    trange = (6, 3)
    river = "River 1D"
    town = "1D Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    # Check the function returns a dictionary with only one entry of River 1D and 1D test station
    assert stations_by_river(s) == {"River 1D":["1D_test_station"]}