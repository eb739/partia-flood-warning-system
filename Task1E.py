from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list

stations = build_station_list()

N_rivers_list = rivers_by_station_number(stations, 6)

print(N_rivers_list)