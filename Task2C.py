from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.stationdata import update_water_levels
from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold
from floodsystem.flood import stations_highest_rel_level

# Build list of stations
stations = build_station_list()

 # Update latest level data for all stations
update_water_levels(stations)

list_highest_rel = stations_highest_rel_level(stations, 10)

for entry in list_highest_rel:
    print("{}, {}".format(entry[0], entry[1]))