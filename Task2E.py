import datetime
from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list
from floodsystem.flood import stations_highest_rel_level
from floodsystem.plot import plot_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import update_water_levels
import matplotlib.pyplot as plt

# Build list of stations
stations = build_station_list()

 # Update latest level data for all stations
update_water_levels(stations)

stations_to_plot = stations_highest_rel_level(stations,5)

stations_to_plot2 = []
for x in stations_to_plot:
    for station in stations:
        if x[0] == station.name:
            stations_to_plot2.append(station)
            break

for station in stations_to_plot2:
    dt = 10
    dates,levels = fetch_measure_levels(station.measure_id,dt=datetime.timedelta(days=dt))
    plot_water_levels(station,dates,levels)
    plt.show()