import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import numpy as np
import matplotlib


def plot_water_levels(station, dates, levels):
    
    plt.plot(dates, levels, label = "Water Level")

    
    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('dates')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)

    plt.axhline(y=station.typical_range[0])

    plt.axhline(y=station.typical_range[1])

    # Display plot
    plt.tight_layout()

    plt.show()



def plot_water_level_with_fit(station, dates, levels, p):
    plt.plot(dates, levels, label = "Water Level")

    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('dates')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)

    plt.axhline(y=station.typical_range[0])

    plt.axhline(y=station.typical_range[1])

    # Display plot
    plt.tight_layout()

    x = matplotlib.dates.date2num(dates)
    y = levels

    # Find coefficients of best-fit polynomial f(x) of degree p
    p_coeff = np.polyfit(x-x[0], y, p)

    # Convert coefficient into a polynomial that can be evaluated,
    # e.g. poly(0.3)
    poly = np.poly1d(p_coeff)

    # Plot polynomial fit at 30 points along interval (note that polynomial
    # is evaluated using the shift x)
    x1 = np.linspace(x[0], x[-1], 30)
    plt.plot(x1, poly(x1 - x[0]))

    # Display plot
    plt.show()