# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from haversine import haversine
from .utils import sorted_by_key



def stations_by_distance(listx,p):
    """This function makes a list of station ids, the town they are located in and their distance from a certain point p."""
    station_distance_list = []
    for entry in listx:
        station_distance_list.append((entry.name, entry.town, haversine(p,entry.coord)))
    station_distance_list = sorted(station_distance_list, key=lambda x: x[2])
    return station_distance_list

def stations_within_radius(listx, centre, r):
    """This function makes a list of station ids within the radius r from the centre"""
    station_radius_list = []
    for entry in listx:
        if haversine(centre,entry.coord) <= r:
            station_radius_list.append(entry.name)
    station_radius_list = sorted(station_radius_list)
    return station_radius_list


def rivers_with_station(listx):
    """This function returns a list of all rivers that have a station on them."""
    set_of_rivers = set()
    for entry in listx:
        set_of_rivers.add(entry.river)
    return set_of_rivers


def stations_by_river(listx):
    """This function returns a dictionary of river names and their stations."""
    set_of_rivers = rivers_with_station(listx)
    dictionary_of_rivers = {}
    for river in set_of_rivers:
        list_of_stations = []
        for entry in listx:
            if entry.river == river:
                list_of_stations.append(entry.name)
        list_of_stations = sorted(list_of_stations)
        dictionary_of_rivers.update({river:list_of_stations})
    return dictionary_of_rivers

def rivers_by_station_number(listx, N):
    """This function returns a list of river name and number of stations based on the greatest number of stations"""
    list_of_tuples = []
    dictionary_of_rivers = stations_by_river(listx)
    for river, stations in dictionary_of_rivers.items():
        list_of_tuples.append((river, len(stations)))
    
    list_of_tuples = sorted_by_key(list_of_tuples, 1, True)

    number_of_stations = list_of_tuples[N-1][1]

    for index in range(N-1, len(list_of_tuples)):
        if list_of_tuples[index][1] < number_of_stations:
            return list_of_tuples[:index]