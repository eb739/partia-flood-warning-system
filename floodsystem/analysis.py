import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import numpy as np
import matplotlib

def polyfit(dates,levels,p):

    x = matplotlib.dates.date2num(dates)
    y = levels

    # Find coefficients of best-fit polynomial f(x) of degree p
    p_coeff = np.polyfit(x-x[0], y, p)

    # Convert coefficient into a polynomial that can be evaluated,
    # e.g. poly(0.3)
    poly = np.poly1d(p_coeff)

    return (poly,x[0])

def assess_risk(station,dt,dates,levels):
    risk = None
    poly = polyfit(dates,levels,4)[0]
    print (str(station.name))
    print ("The polynomial equation is: ")
    print(poly)
    gradient = levels[len(levels)-1]-levels[len(levels)-20]
    print("Gradient: " + str(gradient))
    if gradient > 0:
        increasing = True
        print ("The water level is rising.")
    elif gradient == 0:
        increasing = False
        print ("The water level is not changing.")
    else:
        increasing = False
        print ("The water level is falling.")
    print ("Relative water level: " + str(station.relative_water_level()))
    try:
        if station.relative_water_level() > 1.2 and increasing == False:
            risk = "high"
        elif station.relative_water_level() > 1 and increasing == False:
            risk = "moderate"
        elif increasing == True and station.relative_water_level() > 1.2:
            risk = "severe"
        elif increasing == True and station.relative_water_level() > 1:
            risk = "high"
        elif increasing == True and station.relative_water_level() > 0.8:
            risk = "moderate"
        else:
            risk = "low"
    except:
        None
    print("The risk is " + str(risk))
    print(" ")
    return risk

def just_risk(station,dt,dates,levels):
    risk = None
    gradient = levels[len(levels)-1]-levels[len(levels)-20]
    if gradient > 0:
        increasing = True
    elif gradient == 0:
        increasing = False
    else:
        increasing = False
    try:
        if station.relative_water_level() > 1.2 and increasing == False:
            risk = "high"
        elif station.relative_water_level() > 1 and increasing == False:
            risk = "moderate"
        elif increasing == True and station.relative_water_level() > 1.2:
            risk = "severe"
        elif increasing == True and station.relative_water_level() > 1:
            risk = "high"
        elif increasing == True and station.relative_water_level() > 0.8:
            risk = "moderate"
        else:
            risk = "low"
    except:
        None
    return risk