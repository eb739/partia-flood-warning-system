def stations_level_over_threshold(listx, tol):
    list_of_stations_over_tol = []
    for entry in listx:
        try:
            if entry.relative_water_level() > tol:
                list_of_stations_over_tol.append((entry.name, entry.relative_water_level()))
        except:
            pass
    return sorted(list_of_stations_over_tol, key=lambda x: x[1], reverse = True)
    

def stations_highest_rel_level(listx, N):
    list_of_highest_rel_level = []
    for entry in listx:
        waterlevel = entry.relative_water_level()
        if waterlevel is not None:
            list_of_highest_rel_level.append((entry.name, waterlevel))
    return sorted(list_of_highest_rel_level, key=lambda x: x[1], reverse = True)[0:N]