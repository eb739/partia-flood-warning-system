from floodsystem.station import MonitoringStation
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius


def test_stations_within_radius():

    # Create a station
    s_id = "test1C-s-id"
    m_id = "test1C-m-id"
    label = "1C_test_station"
    coord = (2, 2)
    trange = (6, 3)
    river = "River 1C"
    town = "1C Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    # Check radius is less than 3
    assert stations_within_radius(s,(0,0), 3) == [s]