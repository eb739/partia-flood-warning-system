from floodsystem.station import MonitoringStation
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations


def test_inconsistent_typical_range_stations():

    # Create a station
    s_id = "test1f-s-id"
    m_id = "test1f-m-id"
    label = "1f_test_station"
    coord = (-2.0, 4.0)
    trange = (6, 3)
    river = "River 1F"
    town = "1F Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town

    assert inconsistent_typical_range_stations(s) == False