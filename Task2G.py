import datetime
from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list
from floodsystem.flood import stations_highest_rel_level
from floodsystem.plot import plot_water_levels
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import update_water_levels
from floodsystem.analysis import polyfit
from floodsystem.analysis import assess_risk
from floodsystem.analysis import just_risk
import numpy as np

# Build list of stations
stations = build_station_list()

 # Update latest level data for all stations
update_water_levels(stations)

stations_to_plot = stations_highest_rel_level(stations,5)

stations_to_plot2 = []
for x in stations_to_plot:
    for station in stations:
        if x[0] == station.name:
            stations_to_plot2.append(station)
            break

dates,levels = fetch_measure_levels(station.measure_id,dt=datetime.timedelta(days=2))

for station in stations_to_plot2:
    assess_risk(station,2,dates,levels)

at_risk_stations = []
names_stations_at_risk = []

for station in stations:
    if just_risk(station,2,dates,levels) == "severe" or just_risk(station,2,dates,levels) == "high":
        at_risk_stations.append(station)
        names_stations_at_risk.append(station.name) 
print("The stations with high or severe risks are:")
print (names_stations_at_risk)

towns_at_risk = []

for station in at_risk_stations:
    towns_at_risk.append(station.town)

towns_at_risk = set(towns_at_risk)

print ("The corresponding towns at risk are:")
print(towns_at_risk)